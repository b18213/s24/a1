// S24 Activity:


// >> Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
let number = 3;
let getCube = number**3;
console.log(getCube);
// >> Using Template Literals, print out the value of the getCube variable with a message:
// 		 The cube of <num> is…
cube_Message = console.log(`The cube of ${number} is: ${getCube}`);


// >> Destructure the given array and print out a message with the full address using Template Literals.
	
	const address = ["258", "Washington Ave NW", "California", "90011"];

		
	const [block, street, city, zipCode] = address;
	console.log(`I live at ${block}, ${street}, ${city}, ${zipCode}`);

// >> Destructure the given object and print out a message with the details of the animal using Template Literals.
	
	const animal = {
		name: "Lolong",
		species: "saltwater crocodile",
		weight: "1075 kgs",
		measurement: "20 ft 3 in"
	};

	const{name, species, weight, measurement} = animal;

	console.log(`${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`);

// 		message: <name> was a <species>. He weighed at <weight> with a measurement of <measurement>.

// >> Loop through the given array of characters using forEach, an arrow function and using the implicit return statement to print out each character
	
	const characters = ['Ironman', 'Black Panther', 'Dr. Strange', 'Shang-Chi', 'Falcon']
	characters.forEach(characters =>{
		console.log(`${characters}`)
	});

// >> Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
	class Dog{
		constructor(dogName, dogAge, dogBreed){
			this.dogName = dogName;
			this.dogAge = dogAge;
			this.dogBreed = dogBreed;
		}
	}


// >> Create/instantiate a 2 new object from the class Dog and console log the object
	let myDog = new Dog();
	myDog.dogName = 'Benny';
	myDog.dogAge = '10 months';
	myDog.dogBreed = "Golden Retriever";
	console.log(myDog);

	let myDog2 = new Dog();
	myDog2.dogName = 'Magic';
	myDog2.dogAge = '10 years';
	myDog2.dogBreed = "Aspin";
	console.log(myDog2);
